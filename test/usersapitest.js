'use strict';

const assert = require('chai').assert;
const TweetService = require('./tweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('User API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;

  const tweetService = new TweetService(fixtures.tweetService);

  beforeEach(function () {
    tweetService.login(users[0]);
    //tweetService.deleteAllUsers();
  });

  afterEach(function () {
    //tweetService.deleteAllUsers();
    tweetService.logout();
  });

  test('create a user', function () {
    const returnedUser = tweetService.createUser(newUser);
    assert(_.some([returnedUser], newUser), 'returnedUser must be a superset of newUser');
    assert.isDefined(returnedUser._id);
  });

  test('get user', function () {
    const u1 = tweetService.getLoggedInUser();
    const u2 = tweetService.getUser(u1._id);
    assert.deepEqual(u1, u2);
  });

  test('get invalid user', function () {
    const u1 = tweetService.getUser('1234');
    assert.isNull(u1);
    const u2 = tweetService.getUser('0123456789012345');
    assert.isNull(u2);
  });

  test('get logged in user', function () {
    const u1 = tweetService.getLoggedInUser();
    assert(_.some([u1], users[0]));
  });

  test('update an user', function () {
    let actUser = tweetService.getLoggedInUser();
    const newUser = {};
    newUser.email = 'new@email.com';
    newUser.password = "notSecret";
    tweetService.updateUser(actUser._id, newUser);
    actUser = tweetService.getLoggedInUser();
    assert(actUser.email == newUser.email);
    assert(actUser.password == newUser.password);
  });

  test('delete all users as admin', function () {
    tweetService.logout();
    tweetService.login(users[1]);
    tweetService.deleteAllUsers();
    const allUsers = tweetService.getUsers();
    assert.isNull(allUsers);
  })
});
