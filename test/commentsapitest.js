'use strict';

const assert = require('chai').assert;
const TweetService = require('./tweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Comment API tests', function () {

  let users = fixtures.users;
  let comments = fixtures.comments;
  const newComment = fixtures.newComment;

  const tweetService = new TweetService(fixtures.tweetService);

  beforeEach(function () {
    tweetService.login(users[1]);
    tweetService.deleteAllComments();
    tweetService.logout();
  });

  afterEach(function () {
    tweetService.login(users[1]);
    tweetService.deleteAllComments();
    tweetService.logout();
  });

  test('create a comment', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const comment = newComment;
    const tweets = tweetService.getTweetsOfUser(returnedUser._id);
    tweetService.createComment(tweets[0]._id, comment);
    const returnedComment = tweetService.getCommentsOfUser(returnedUser._id);
    assert(returnedComment.length, 1);
    assert(_.some(returnedComment), comment);
    tweetService.logout();
  });

  test('create multiple comments', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweets = tweetService.getTweetsOfUser(returnedUser._id);
    for (let i = 0; i < comments.length; i++) {
      tweetService.createComment(tweets[0]._id, comments[i]);
    }

    const returnedComments = tweetService.getCommentsOfUser(returnedUser._id);
    assert.equal(returnedComments.length, comments.length);
    for (let i = 0; i < tweets.length; i++) {
      assert(_.some([returnedComments[i]]), comments[i], 'returned tweet must be a superset of tweet');
    }
    tweetService.logout();
  });

  test('delete all comments of user as other user', function () {

    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweets = tweetService.getAllTweets();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[3]);

    const d1 = tweetService.getCommentsOfUser(returnedUser._id);
    assert.equal(d1.length, comments.length);
    tweetService.deleteCommentsOfUser(returnedUser._id);
    const d2 = tweetService.getCommentsOfUser(returnedUser._id);
    assert.equal(d2.length, 3);
    const d3 = tweetService.getAllComments();
    assert(d3.length > 0);
    tweetService.logout();
  });

  test('delete all comments of user as admin', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweets = tweetService.getAllTweets();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[1]);

    const d1 = tweetService.getCommentsOfUser(returnedUser._id);
    assert.equal(d1.length, comments.length);
    tweetService.deleteCommentsOfUser(returnedUser._id);
    const d2 = tweetService.getCommentsOfUser(returnedUser._id);
    assert.equal(d2.length, 0);
    const d3 = tweetService.getAllComments();
    assert(d3.length > 0);
    tweetService.logout();
  });

  test('delete all comments of tweet as user of other user', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const tweets = tweetService.getAllTweets();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[3]);

    const d1 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d1.length, 12);
    tweetService.deleteCommentsOfTweet(tweets[0]._id);
    const d2 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d2.length, 12);
    tweetService.logout();
  });

  test('delete all comments of tweet as user as author', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const tweets = tweetService.getAllTweets();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[0]);

    const d1 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d1.length, 12);
    tweetService.deleteCommentsOfTweet(tweets[0]._id);
    const d2 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d2.length, 0);
    tweetService.logout();
  });

  test('delete all comments of tweet as admin', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const tweets = tweetService.getAllTweets();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[1]);

    const d1 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d1.length, 12);
    tweetService.deleteCommentsOfTweet(tweets[0]._id);
    const d2 = tweetService.getCommentsOfTweet(tweets[0]._id);
    assert.equal(d2.length, 0);
    tweetService.logout();
  });

  test('delete all comments as user', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweets = tweetService.getTweetsOfUser(returnedUser._id);
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[0]);

    const d = tweetService.getAllComments();
    assert(d.length !== 0);

    tweetService.deleteAllComments();
    const d1 = tweetService.getAllComments();
    assert.equal(d1.length, 12);
    tweetService.logout();
  });

  test('delete all comments as admin', function () {
    tweetService.login(users[0]);
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweets = tweetService.getTweetsOfUser(returnedUser._id);
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < comments.length; j++) {
        tweetService.createComment(tweets[0]._id, comments[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[1]);

    const d = tweetService.getAllComments();
    assert(d.length !== 0);

    tweetService.deleteAllComments();
    const d1 = tweetService.getAllComments();
    assert.equal(d1.length, 0);
    tweetService.logout();
  });
});
