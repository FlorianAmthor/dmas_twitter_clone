'use strict';

const SyncHttpClient = require('./sync-http-client');
//const baseUrl = 'http://localhost:4000';

class TweetService {

  constructor(baseUrl) {
    this.httpClient = new SyncHttpClient(baseUrl);
  }

  //User functions

  getUsers() {
    return this.httpClient.get('/api/users');
  }

  getUser(id) {
    return this.httpClient.get('/api/users/' + id);
  }

  createUser(newUser) {
    return this.httpClient.post('/api/users', newUser);
  }

  getLoggedInUser(){
    return this.httpClient.get('/api/users/getLoggedInUser');
  }

  updateUser(id, user){
    return this.httpClient.post('/api/users/'+ id +'/update', user);
  }

  deleteOneUser(id) {
    return this.httpClient.delete('/api/users/' + id);
  }

  deleteAllUsers() {
    return this.httpClient.delete('/api/users');
  }

  //Tweet functions

  getTweet(id) {
    return this.httpClient.get('/api/tweets/' + id);
  }

  getTweetsOfUser(id) {
    return this.httpClient.get('/api/tweets/users/' + id);
  }

  getAllTweets() {
    return this.httpClient.get('/api/tweets');
  }

  createTweet(tweet) {
    return this.httpClient.post('/api/tweets', tweet);
  }

  deleteAllTweets() {
    return this.httpClient.delete('/api/tweets');
  }

  deleteTweetsOfUser(id) {
    return this.httpClient.delete('/api/tweets/users/' + id );
  }

  deleteOneTweet(id){
    return this.httpClient.delete('/api/tweets' + id);
  }

  //Comment functions

  getComment(id) {
    return this.httpClient.get('/api/comments/' + id);
  }

  getCommentsOfUser(id) {
    return this.httpClient.get('/api/comments/users/' + id);
  }

  getCommentsOfTweet(id) {
    return this.httpClient.get('/api/comments/tweets/' + id);
  }

  getAllComments() {
    return this.httpClient.get('/api/comments');
  }

  createComment(id, comment) {
    return this.httpClient.post('/api/comments/tweets/' + id, comment);
  }

  deleteAllComments() {
    return this.httpClient.delete('/api/comments');
  }

  deleteCommentsOfUser(id) {
    return this.httpClient.delete('/api/comments/users/' + id);
  }

  deleteCommentsOfTweet(id) {
    return this.httpClient.delete('/api/comments/tweets/' + id);
  }

  deleteOneComment(id){
    return this.httpClient.delete('/api/comments' + id);
  }

  //authenticate functions

  login(user) {
    return this.httpClient.setAuth('/api/users/authenticate', user);
  }

  logout() {
    this.httpClient.clearAuth();
  }

}

module.exports = TweetService;
