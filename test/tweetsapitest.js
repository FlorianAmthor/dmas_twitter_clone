'use strict';

const assert = require('chai').assert;
const TweetService = require('./tweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweet API tests', function () {

  let users = fixtures.users;
  let tweets = fixtures.tweets;
  const newTweet = fixtures.newTweet;

  const tweetService = new TweetService(fixtures.tweetService);

  beforeEach(function () {
    tweetService.login(users[1]);
    tweetService.deleteAllTweets();
    tweetService.logout();
    tweetService.login(users[0]);
  });

  afterEach(function () {
    tweetService.logout();
    tweetService.login(users[1]);
    tweetService.deleteAllTweets();
    tweetService.logout();
  });

  test('create a tweet', function () {
    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    const tweet = newTweet;
    tweet.author = returnedUser._id;
    tweetService.createTweet(tweet);
    const returnedTweet = tweetService.getTweetsOfUser(returnedUser._id);
    assert(returnedTweet.length, 1);
    assert(_.some(returnedTweet), tweet);
  });

  test('create multiple tweets', function () {

    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(tweets[i]);
    }

    const returnedTweets = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);
    for (let i = 0; i < tweets.length; i++) {
      assert(_.some([returnedTweets[i]]), tweets[i], 'returned tweet must be a superset of tweet');
    }
  });

  test('delete all tweets of user as user', function () {

    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < tweets.length; j++) {
        tweetService.createTweet(tweets[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[0]);

    const d1 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d1.length, tweets.length);
    tweetService.deleteTweetsOfUser(returnedUser._id);
    const d2 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d2.length, 0);
    const d3 = tweetService.getAllTweets();
    assert(d3.length > 0);
  });

  test('delete all tweets of user as other user', function () {

    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < tweets.length; j++) {
        tweetService.createTweet(tweets[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[3]);

    const d1 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d1.length, tweets.length);
    tweetService.deleteTweetsOfUser(returnedUser._id);
    const d2 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d2.length, tweets.length);
    const d3 = tweetService.getAllTweets();
    assert(d3.length > 0);
  });

  test('delete all tweets of user as admin', function () {

    const allUsers = tweetService.getUsers();
    const returnedUser = allUsers[0];
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < tweets.length; j++) {
        tweetService.createTweet(tweets[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[1]);

    const d1 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d1.length, tweets.length);
    tweetService.deleteTweetsOfUser(returnedUser._id);
    const d2 = tweetService.getTweetsOfUser(returnedUser._id);
    assert.equal(d2.length, 0);
    const d3 = tweetService.getAllTweets();
    assert(d3.length > 0);
  });

  test('delete all tweets as user', function () {

    const allUsers = tweetService.getUsers();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < tweets.length; j++) {
        tweetService.createTweet(tweets[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[0]);

    const d = tweetService.getAllTweets();
    assert(d.length !== 0);

    tweetService.deleteAllTweets();
    const d1 = tweetService.getAllTweets();
    assert.equal(d1.length, 12);
  });

  test('delete all tweets as admin', function () {

    const allUsers = tweetService.getUsers();
    tweetService.logout();
    for (let i = 0; i < allUsers.length; i++) {
      tweetService.login(allUsers[i]);
      for (let j = 0; j < tweets.length; j++) {
        tweetService.createTweet(tweets[j]);
      }
      tweetService.logout();
    }

    tweetService.login(users[1]);

    const d = tweetService.getAllTweets();
    assert(d.length !== 0);

    tweetService.deleteAllTweets();
    const d1 = tweetService.getAllTweets();
    assert.equal(d1.length, 0);
  });
});
