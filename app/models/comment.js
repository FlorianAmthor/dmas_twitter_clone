'use strict';

const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  date: { type: Date, default: Date.now() },
  content: { type: String, required: true },
  tweetId: { type: mongoose.Schema.Types.ObjectId, ref: 'Tweet' },
});

const Comment = mongoose.model('Comment', commentSchema);
module.exports = Comment;
