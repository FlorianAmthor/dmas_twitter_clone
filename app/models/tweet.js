'use strict';

const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  date: { type: Date, default: Date.now() },
  content: { type: String, required: true },
});

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;

