'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: {
    type: String,
    lowercase: true
  },
  password: String,
  isAdmin: Boolean,
});

const User = mongoose.model('User', userSchema);
module.exports = User;
