const jwt = require('jsonwebtoken');
const User = require('../models/user');
const utils = require('../api/utils');

exports.createToken = function (user) {
  return jwt.sign({id: user._id, email: user.email, isAdmin: user.isAdmin}, 'secretpasswordnotrevealedtoanyone', {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
};

exports.decodeToken = function (token) {
  let userInfo = {};
  try {
    const decoded = jwt.verify(token, 'secretpasswordnotrevealedtoanyone');
    userInfo.userId = decoded.id;
    userInfo.email = decoded.email;
    userInfo.isAdmin = decoded.isAdmin;
  } catch (e) {
  }

  return userInfo;
};

exports.validate = function (decoded, request, callback) {
  User.findOne({_id: decoded.id}).then(user => {
    if (user != null) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  }).catch(err => {
    callback(null, false);
  });
};

exports.checkPermission = function (userId, token) {
  try{
    const decodedToken = utils.decodeToken(token);
    return userId == decodedToken.userId || decodedToken.isAdmin ? true : false;
  }catch (e){

  }
  };

