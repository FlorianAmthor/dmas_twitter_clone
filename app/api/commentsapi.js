'use strict';

const Comment = require('../models/comment');
const Tweet = require('../models/tweet');
const Boom = require('boom');
const utils = require('./utils');

exports.findOneComment = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Comment.findOne({_id: request.params.id}).populate('author').then(comment => {
      reply(comment);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

exports.findCommentsOfTweet = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Comment.find({tweetId: request.params.id}).populate('tweetId').then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

exports.findCommentsOfUser = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Comment.find({author: request.params.id}).populate('author').then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

exports.findAllComments = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Comment.find({}).then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.createComment = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const comment = new Comment(request.payload);
    comment.tweetId = request.params.id;

    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    comment.author = utils.decodeToken(token).userId;

    comment.save().then(newComment => {
      reply(newComment).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error creating comment'));
    });
  },

};

exports.deleteOneComment = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    Comment.findOne({_id: request.params.id}).then(returnedComment => {
      if (utils.checkPermission(returnedComment.author, token)) {
        Comment.remove({_id: request.params.id}).then(result => {
          reply().code(204);
        }).catch(err => {
          reply(Boom.badImplementation('error removing comment'));
        });
      } else {
        reply(Boom.unauthorized('unauthorized'));
      }
    }).catch(err => {
      reply(Boom.notFound('content not found'));
    });
  },
};

exports.deleteCommentsOfUser = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    if (utils.checkPermission(request.params.id, token)) {
      Comment.remove({author: request.params.id}).then(result => {
        reply().code(204);
      }).catch(err => {
        reply(Boom.badImplementation('error removing Comments'));
      });
    } else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },
};

exports.deleteCommentsOfTweet = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    Tweet.findOne({_id: request.params.id}).then(returnedTweet => {
      if (utils.checkPermission(returnedTweet.author, token)) {
        Comment.remove({tweetId: request.params.id}).then(result => {
          reply().code(204);
        }).catch(err => {
          reply(Boom.badImplementation('error removing Comments'));
        });
      } else {
        reply(Boom.unauthorized('unauthorized'));
      }
    }).catch(err => {
      reply(Boom.notFound('content not found'));
    });
  },
};

exports.deleteAllComments = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    if (utils.checkPermission(null, token)) {
      Comment.remove({}).then(err => {
        reply().code(204);
      }).catch(err => {
        reply(Boom.badImplementation('error removing Comments'));
      });
    } else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },
};