'use strict';

const User = require('../models/user');
const Tweet = require('../models/tweet');
const Comment = require('../models/comment');
const Boom = require('boom');
const utils = require('./utils.js');

exports.find = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.find({}).exec().then(users => {
      reply(users);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.findOne({_id: request.params.id}).then(user => {
      if (user != null) {
        reply(user);
      }
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};
;

exports.create = {

  auth: false,
  handler: function (request, reply) {
    const user = new User(request.payload);
    user.save().then(newUser => {
      reply(newUser).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error creating User'));
    });
  },

};


exports.update = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];
    if (utils.checkPermission(request.params.id, token)) {
      const editedUser = request.payload;
      User.findOne({_id: utils.decodeToken(token).userId}).then(user => {
        user.email = editedUser.email;
        user.password = editedUser.password;
        user.save().then(newUser => {
          reply(newUser).code(201);
        }).catch(err => {
          reply(Boom.badImplementation('error updating User'));
        });
      }).catch(err => {
        reply(Boom.badImplementation('Cannot find User'));
      });
    }
    else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },

};

exports.getLoggedInUser = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    let user = utils.decodeToken(request.headers.authorization.split(' ')[1]);
    User.findOne({_id: user.userId}).then(user => {
      if (user != null) {
        reply(user);
      }
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};
;

exports.deleteAll = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    if (utils.checkPermission(null, token)) {
      User.remove({}).then(err => {
        Tweet.remove({}).then(err => {
          Comment.remove({}).then(err => {
          }).catch(err => {
            reply(Boom.badImplementation('error removing comments'));
          });
        }).catch(err => {
          reply(Boom.badImplementation('error removing tweets'));
        });
        reply().code(204);
      }).catch(err => {
        reply(Boom.badImplementation('error removing Users'));
      });
    }
    else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },

};

exports.deleteOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];
    if (utils.checkPermission(request.params.id), token) {
      User.remove({_id: request.params.id}).then(user => {
        Tweet.find({author: request.params.id}).then(tweets => {
          Tweet.remove({author: request.params.id}).then(result => {
            for (let tweet of tweets) {
              Comment.remove({tweetId: tweet._id}).then(result => {
                Comment.remove({author: request.params.id}).then(result => {
                }).catch(err => {
                  reply(Boom.badImplementation('error removing comments of user'));
                });
              }).catch(err => {
                reply(Boom.badImplementation('error removing comments'));
              });
            }
          }).catch(err => {
            reply(Boom.badImplementation('error removing Tweets'));
          });
        });
        reply(User).code(204);
      }).catch(err => {
        reply(Boom.notFound('id not found'));
      });
    }
    else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },

};

exports.authenticate = {
  auth: false,
  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({email: user.email}).then(foundUser => {
      if (foundUser && foundUser.password === user.password) {
        const token = utils.createToken(foundUser);
        reply({success: true, token: token}).code(201);
      } else {
        reply({success: false, message: 'Authentication failed. User not found.'}).code(201);
      }
    }).catch(err => {
      reply(Boom.notFound('internal db failure'));
    });
  },

};
