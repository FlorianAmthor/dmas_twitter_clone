'use strict';

const Tweet = require('../models/tweet');
const Comment = require('../models/comment');
const Boom = require('boom');
const utils = require('./utils');

exports.findOneTweet = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.find({_id: request.params.id}).populate('author').populate('comments').then(tweet => {
      reply(tweet);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};


exports.findAllTweets = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.find({}).sort('-date').populate('author').then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

exports.findTweetsofUser = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.find({author: request.params.id}).sort('-date').populate('author').then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.createTweet = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const tweet = new Tweet(request.payload);

    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    tweet.author = utils.decodeToken(token).userId;

    tweet.save().then(newTweet => {
      Tweet.findOne({_id: newTweet._id}).populate('author').then(returnedTweet => {
        reply(returnedTweet).code(201);
      }).catch(err => {
        reply(Boom.badImplementation('error making tweet'));
      });
    }).catch(err => {
      reply(Boom.badImplementation('error making tweet'));
    });
  },

};

exports.deleteOneTweet = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    Tweet.findOne({_id: request.params.id}).then(returnedTweet => {
      if (utils.checkPermission(returnedTweet.author, token)) {
        Tweet.remove({_id: request.params.id}).then(result => {
          Comment.remove({tweetId: request.params.id}).then(result => {
            reply().code(204);
          }).catch(err => {
            reply(Boom.badImplementation('error removing comments of tweet'));
          });
        }).catch(err => {
          reply(Boom.badImplementation('error removing Tweets'));
        });
      } else {
        reply(Boom.unauthorized('unauthorized'));
      }
    }).catch(err => {
      reply(Boom.notFound('content not found'));
    });
  },
};

exports.deleteTweetsOfUser = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    if (utils.checkPermission(request.params.id, token)) {
      Tweet.find({author: request.params.id}).then(tweets => {
        Tweet.remove({author: request.params.id}).then(result => {
          for (let tweet of tweets) {
            Comment.remove({tweetId: tweet._id}).then(result => {
            }).catch(err => {
              reply(Boom.badImplementation('error removing comments'));
            });
          }
          reply().code(204);
        }).catch(err => {
          reply(Boom.badImplementation('error removing Tweets'));
        });
      });
    } else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },
};

exports.deleteAllTweets = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const authorization = request.headers.authorization;
    const token = authorization.split(' ')[1];

    if (utils.checkPermission(null, token)) {
      Tweet.remove({}).then(err => {
        Comment.remove({}).then(err => {
          reply().code(204);
        }).catch(err => {
          reply(Boom.badImplementation('error removing comment'));
        })
      }).catch(err => {
        reply(Boom.badImplementation('error removing Tweets'));
      });
    } else {
      reply(Boom.unauthorized('unauthorized'));
    }
  },

};