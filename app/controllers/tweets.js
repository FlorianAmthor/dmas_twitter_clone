'use strict';

const Tweet = require('../models/tweet');
const User = require('../models/user');
const Comment = require('../models/comment');
const Joi = require('joi');

exports.home = {

  handler: function (request, reply) {
    User.find({}).sort([['lastName', 'ascending']]).then(users => {
      Tweet.find({}).populate("author").sort([['date', 'descending']]).then(tweets => {
        reply.view('home', {
          title: 'Dashboard',
          tweets: tweets,
          users: users,
        });
      }).catch(err => {
        reply.redirect('/');
      });
    }).catch(err => {
      reply.redirect('/');
    })
  },

};

exports.deleteOne = {

  validate: {

    options: {
      abortEarly: false,
    },

    payload: {
      _id: Joi.string().required(),
    },
  },

  handler: function (request, reply) {
    const data = request.payload;
    Tweet.findOne({_id: request.payload._id}).then(returnedTweet => {
      Tweet.remove({_id: returnedTweet._id}).then(result => {
        Comment.remove({tweetId: request.params.id}).then(result => {
          reply().code(204).redirect('/home');
        }).catch(err => {
          reply.redirect('/home');
        });
      }).catch(err => {
        reply.redirect('/home');
      });
    }).catch(err => {
      reply.redirect('/home');
    });
  }
};

exports.deleteAll = {

  handler: function (request, reply) {
    Tweet.remove({}).then(err => {
      Comment.remove({}).then(err => {
        reply().code(204).redirect('/home');
      }).catch(err => {
        reply('/home');
      })
    }).catch(err => {
      reply('/home');
    });
  }
};
