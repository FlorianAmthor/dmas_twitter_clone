'use strict';

const User = require('../models/user');
const Tweet = require('../models/tweet');
const Comment = require('../models/comment');

const Joi = require('joi');

exports.main = {

  auth: false,
  handler: function (request, reply) {
    reply.view('main', {title: 'Twitter-Clone Admin View'});
  },

};

exports.login = {
  auth: false,

  handler: function (request, reply) {
    reply.view('login', {title: 'Login to Twitter-Clone Admin View'});
  },

};

exports.logout = {

  auth: false,
  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },

};

exports.register = {
  auth: false,

  validate: {

    options: {
      abortEarly: false,
    },

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('main', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const user = new User(request.payload);

    user.save().then(newUser => {
      reply.redirect('/');
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

exports.authenticate = {
  auth: false,

  validate: {

    options: {
      abortEarly: false,
    },

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('main', {
        title: 'Log in error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({email: user.email}).then(foundUser => {
      if (foundUser && foundUser.password === user.password && foundUser.isAdmin === true) {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        reply.redirect('/home');
      } else {
        reply.redirect('/');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

exports.viewSettings = {

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({email: userEmail}).then(foundUser => {
      reply.view('settings', {title: 'Edit Account Settings', user: foundUser});
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

exports.updateSettings = {

  validate: {

    options: {
      abortEarly: false,
    },

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'User settings error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const editedUser = request.payload;
    const loggedInUserEmail = request.auth.credentials.loggedInUser;

    User.findOne({email: loggedInUserEmail}).then(user => {
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      user.password = editedUser.password;
      return user.save();
    }).then(user => {
      reply.view('settings', {title: 'Edit Account Settings', user: user});
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

exports.deleteOne = {

  validate: {

    options: {
      abortEarly: false,
    },

    payload: {
      _id: Joi.string().required(),
    },
  },

  handler: function (request, reply) {
    User.findOne({_id: request.payload._id}).then(returnedUser => {
      User.remove({_id: returnedUser._id}).then(res => {
        Tweet.remove({author: returnedUser._id}).then(result => {
          reply().code(204).redirect('/home');
        }).catch(err => {
          reply.redirect('/home');
        });
      }).catch(err => {
        reply.redirect('/home');
      });
    }).catch(err => {
      reply.redirect('/home');
    });
  }
};

exports.deleteAll = {

  handler: function (request, reply) {
    User.remove({isAdmin: false}).then(err => {
      Tweet.remove({}).then(err => {
        Comment.remove({}).then(err => {
          reply().code(204).redirect('/home');
        }).catch(err => {
          reply('/home');
        })
      }).catch(err => {
        reply('/home');
      })
    }).catch(err => {
      reply('/home');
    });
  }
};
