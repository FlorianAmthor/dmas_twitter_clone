const UsersApi = require('./app/api/usersapi');
const TweetsApi = require('./app/api/tweetsapi');
const CommentsApi = require('./app/api/commentsapi');

module.exports = [

  { method: 'GET', path: '/api/users', config: UsersApi.find },
  { method: 'GET', path: '/api/users/{id}', config: UsersApi.findOne },
  { method: 'GET', path: '/api/users/getLoggedInUser', config: UsersApi.getLoggedInUser },
  { method: 'POST', path: '/api/users', config: UsersApi.create },
  { method: 'POST', path: '/api/users/authenticate', config: UsersApi.authenticate },
  { method: 'POST', path: '/api/users/{id}/update', config: UsersApi.update },
  { method: 'DELETE', path: '/api/users/{id}', config: UsersApi.deleteOne },
  { method: 'DELETE', path: '/api/users', config: UsersApi.deleteAll },

  { method: 'GET', path: '/api/tweets', config: TweetsApi.findAllTweets },
  { method: 'GET', path: '/api/tweets/{id}', config: TweetsApi.findOneTweet },
  { method: 'GET', path: '/api/tweets/users/{id}', config: TweetsApi.findTweetsofUser },
  { method: 'POST', path: '/api/tweets', config: TweetsApi.createTweet },
  { method: 'DELETE', path: '/api/tweets/{id}', config: TweetsApi.deleteOneTweet },
  { method: 'DELETE', path: '/api/tweets', config: TweetsApi.deleteAllTweets },
  { method: 'DELETE', path: '/api/tweets/users/{id}', config: TweetsApi.deleteTweetsOfUser },

  { method: 'GET', path: '/api/comments', config: CommentsApi.findAllComments },
  { method: 'GET', path: '/api/comments/{id}', config: CommentsApi.findOneComment },
  { method: 'GET', path: '/api/comments/tweets/{id}', config: CommentsApi.findCommentsOfTweet},
  { method: 'GET', path: '/api/comments/users/{id}', config: CommentsApi.findCommentsOfUser},
  { method: 'POST', path: '/api/comments/tweets/{id}', config: CommentsApi.createComment },
  { method: 'DELETE', path: '/api/comments/{id}', config: CommentsApi.deleteOneComment },
  { method: 'DELETE', path: '/api/comments', config: CommentsApi.deleteAllComments },
  { method: 'DELETE', path: '/api/comments/users/{id}', config: CommentsApi.deleteCommentsOfUser },
  { method: 'DELETE', path: '/api/comments/tweets/{id}', config: CommentsApi.deleteCommentsOfTweet },
];
